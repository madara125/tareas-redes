#Universidad Nacional Autonoma de Mexico.
#Facultad de Ciencias & Ciencias de la Computacion.
## Redes de Computadoras 2019-2.
## Martinez Sanchez Jose Manuel.
### Practica 2.

En el directorio ```/src``` hacer ```make``` para compilar.  Ejecutar con ```./servidor [puerto]``` y ```./cliente [host] [puerto]```.

Ejemplo de ejecucion:

![Drag Racing](/src/img/s1.png)
Para salir, escribir "EOF" en el cliente.
