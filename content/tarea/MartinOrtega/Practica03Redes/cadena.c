#include <stdio.h>
#include <ctype.h>
#include <string.h>

void readFile(char *file);

int main(int argv,char *argc[])
{
  for(int tmp = argv-1; tmp>0; tmp--){
    readFile(argc[tmp]);
    }
}

void readFile(char *file)
{
  char contenido;
  FILE *fp = fopen(file,"rb");
  while((contenido = fgetc(fp)) != EOF){
    if(isgraph(contenido)){
      printf("%c",contenido);
    }
  }
  fclose(fp);
}
